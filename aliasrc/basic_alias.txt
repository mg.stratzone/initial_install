alias tmt='_tmt() { tar -c --use-compress-program="pigz --best --recursive" -f "${1}_$(date +%F_%H%M%S).txz" "$1"; }; _tmt'
alias ipex="curl ifconfig.me"
alias tb="nc termbin.com 9999"
