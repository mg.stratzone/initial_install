# Alias

## How to install
* Basic Alias
```bash
source <(curl -s https://gitlab.com/strategiczone/initial_install/-/raw/master/aliasrc/basic_alias.txt | tee -a ~/.aliasrc)
```

* Docker Alias
```bash
source <(curl -s https://gitlab.com/strategiczone/initial_install/-/raw/master/aliasrc/docker_alias.txt | tee -a ~/.aliasrc)
```
