# Push Files to tranfer.sh with an alias
## How to install
```bash
source <(curl -s https://gitlab.com/strategiczone/initial_install/-/raw/master/transfersh-alias/transfersh_install.sh)
```

## How to use
### Add this script to your alias
```
alias trs="~/.transfersh/transfersh-alias.sh "
```

### Tranfer a file
```
trs my_file.md
```

### Crypt with gpg (with password) and tranfer
```
trs crypt my_file.md
```

### Crypt and set max stored days
```
trs crypt 5 my_file.md
```
### Take a screnshot and push it crypted or not ;-)
```
trs crypt screenshot
```
### Clipboard
In order to have this function, install in your OS `xclip` and set $DISPLAY (default) variable
