#!/usr/bin/env bash
#title		:transfer-alias.sh
#description	:file to transfer.sh 
#author		:Valeriu Stinca
#email		:sys@0hz.io
#date		:20180419
#version	:2
#notes		:
#===================
set -e
TS_SERVER="https://transfer.sh"
max_days=false
my_file=false
encrypt=false
file_name=false

help() {
	echo -e "Usage:\ntransfer /tmp/test.md\ntransfer /tmp/test.md encrypt #encrypt with GPG (password)\ntransfer /tmp/test.md 5 #File stored for 5 days | Default(max) 14 days"
	exit 0
}
error() {
	echo -e "${1}"
	exit 1
}
encrypt() {
	file_name="${file_name}.gpg"
	/usr/bin/gpg -ac -o /tmp/"${file_name}" "${my_file}"
	my_file=/tmp/"${file_name}"
	
}
screenshot() {
	if [[ -n $(command -v import) && -n ${DISPLAY} ]]
	then
		my_file="$(mktemp --suffix='.png')"
		import -w root ${my_file}
		file_name="$(basename ${my_file})"
	else
		error "Please check your environment!"	
fi
}
push() {
	if ${encrypt}; then
		encrypt
	fi
	my_url=$(curl -H "Max-Days: ${max_days}" --progress-bar --upload-file "${my_file}" "$TS_SERVER/${file_name}")
	if [[ -n $(command -v xclip) && -n ${DISPLAY} ]]
	then
		echo "${my_url}" | xclip -selection clipboard
		echo -e "URL copied to your clipboard \n${my_url}"
	else
		echo "URL: ${my_url}"
	fi
}
checkfile() {
	if [ -f "${1}" ]
	then
		my_file="${1}"
		file_name=$(basename "$my_file" | sed -e 's/[^a-zA-Z0-9._-]/-/g')
	else
		error "check your file!"
	fi
}
# start here
if [ ${#} -gt 3 -o ${#} -lt 1 ]
then
	help
fi 
my_vars=($@)

for var in "${my_vars[@]}"
do
	case ${var} in
		encrypt )
			encrypt=true ;;
		[1-9]|1[1-4] )
			max_days=${var} ;;
		screenshot)
			screenshot ;;
		*)
			if ! ${file_name}; then
				echo $var
				checkfile ${var}
			fi
			;;
	esac
done
push
exit 0
