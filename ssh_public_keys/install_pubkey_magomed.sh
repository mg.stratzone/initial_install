#!/usr/bin/env bash
#title		    : install_pubkey_magomed.sh
#description	: Installation de la cle public de Magomed Gamadaev
#author		    : Valeriu Stinca
#email		    : ts@strategic.zone
#date		    : 26112019
#version	    : 0.3
#notes	    	:
#===================

key_url="https://gitlab.com/strategiczone/initial_install/-/raw/master/ssh_public_keys/id_ecdsa_mg-laptop.pub"

mkdir -p ~/.ssh
grep -q "$(curl --silent "${key_url}")" ~/.ssh/authorized_keys || curl --silent "${key_url}" | tee -a ~/.ssh/authorized_keys
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
