# SSH Public Keys
## How to install

Magomed
```bash
source <(curl -s https://gitlab.com/strategiczone/initial_install/-/raw/master/ssh_public_keys/install_pubkey_magomed.sh)
```
Valeriu
```bash
source <(curl -s https://gitlab.com/strategiczone/initial_install/-/raw/master/ssh_public_keys/install_pubkey_valerius.sh)
```
Raphael
```bash
source <(curl -s https://gitlab.com/strategiczone/initial_install/-/raw/master/ssh_public_keys/install_pubkey_raphael.sh)
```
