#!/usr/bin/env bash
#title		    : install_pubkey_gabriel.sh
#description	: Installation de la cle public Raphael Lalung
#author		    : Magomed Gamadaev
#email		    : ts@strategic.zone
#date		    : 20210212
#version	    : 0.2
#notes	    	:
#===================

key_url="https://gitlab.com/strategiczone/initial_install/-/raw/master/ssh_public_keys/id_rsa_raphael_lalung.pub"

mkdir -p ~/.ssh
grep -q "$(curl --silent "${key_url}")" ~/.ssh/authorized_keys || curl --silent "${key_url}" | tee -a ~/.ssh/authorized_keys
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
