#!/usr/bin/env bash
: <<COMMENTBLOCK
title			    :Install ohMyZSH
description   :
author        :Valeriu Stinca
email         :ts@strategic.zone
date          :20210724
version			  :0.5
notes			    :
================================
COMMENTBLOCK


set -e

#check if commandes exists
if ! [ -x "$(command -v git)" ]; then
  echo 'Error: please install git.' >&2
  exit 1
fi
if ! [ -x "$(command -v zsh)" ]; then
	echo 'Error: please install zsh.' >&2
	exit 1
fi

# Creation config de zsh avec oh-my-zsh
git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
if [ -f ~/.zshrc ]; then
	cp ~/.zshrc ~/.zshrc.orig
fi
touch ~/.zshrc
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
sed -i 's/^ZSH_THEME.*$/ZSH_THEME="agnoster"/' ~/.zshrc
sed -i 's/\#\ ENABLE_CORRECTION=\"true\"/ENABLE_CORRECTION=\"true\"/g' ~/.zshrc

if [ ! -f ~/.aliasrc ]; then
	touch ~/.aliasrc
fi

echo 'source ~/.aliasrc' | tee -a ~/.zshrc
sudo chsh  "${USER}" -s /bin/zsh 
echo "Your default SHELL changed with: chsh $USER -s /bin/zsh"
